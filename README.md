codeRetreat 2015 NodeJs
========

Written in Typescript
visualStudio compiles it automatically to .js files
If you do not have VisualStudio, then you will need to install TypeScript compiler
and add watch to compile on safe.

When you are using VisualStudio then do not use watch for running your tests suites but rather run them manually.
At least in my case the test runner crashed randomly when it watched generated .js files.

npm install

npm run test