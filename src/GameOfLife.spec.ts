﻿var gameOfLife = require('./GameOfLife');

describe("GameOfLife", () => {
    
    it("should get 10", () => {
        var instance = new gameOfLife();
        var result = instance.getTen();

        expect(result).toBe(10);
    });

});
